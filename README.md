
# Get quota by user for a given DDM (example: CERN-PROD_LOCALGROUPDISK)

Go to https://rucio-ui.cern.ch/r2d2/manage_quota

Choose ```RSE``` and press ```Select```.

Select table and copy to a spreadsheet (example: https://docs.google.com/spreadsheets/)

Export it as a CSV file (for Google Sheets, go to Download and select 'Comma Separated Values (.csv)')

# Make plots and rankings

Select corresponding ```InputFile``` in ```MakeRankings.py``` and run script in the following way:

```
python MakeRankings.py
```
