
InputFile  = 'Quotas_24112021.csv'

TotalQuota = '1.24 PB' # approx.

######################################################
# DO NOT MODIFY (below this line)
######################################################

import pandas as pd
import numpy  as np

header = ['user','quota','used','available','#files']

# Dict to know how to convert from a given unit to another one
Units = {
  'B'  : 1,
  'kB' : 1000,
  'MB' : 1000000,
  'GB' : 1000000000,
  'TB' : 1000000000000,
  'PB' : 1000000000000000,
  'EB' : 1000000000000000000,
}

# Convert to a given unit
def convertQuota(string:str,target:str) -> float:
  """Extract unit and convert to target unit"""
  array  = string.split(' ')
  value  = float(array[0])
  unit   = array[1]
  factor = Units[unit]/Units[target]
  quota = round(value*factor,2)
  return quota if quota > 0 else 0

# Read CSV
data         = pd.read_csv(InputFile, names=header, na_values='?', comment='\t', sep=',')
data['used'] = data['used'].apply(lambda x: convertQuota(x,'GB'))

# Get how much of the total quota was used
UsedByUser               = data['used'].copy()
Nusers                   = len(UsedByUser)
UsedByUserArray          = UsedByUser.values.reshape(Nusers,-1)

# Print quota usage
Total = round(convertQuota(TotalQuota,'TB'),2)
Used  = round(np.sum(UsedByUserArray)/1000,2)
print('################################################')
print('{} of {} [TB] ({} %) is being used'.format(Used,Total,round(100*Used/Total,2)))

# Print first 10 users using more quota
print('################################################')
print('Ranking of first 20 users using more quota (on TB):')
TopUsers         = data.nlargest(20, 'used')[['user','used']]
TopUsers['used'] = TopUsers['used'].apply(lambda x: x*0.001)
print(TopUsers)

# Show users which don't have quota (left CERN?) but still use quota
print('################################################')
print('Users with no quota using quota (on GB) anyway:')
Data          = data.copy()
Data['quota'] = Data['quota'].apply(lambda x : int(convertQuota(x,'GB')))
Data = Data.loc[Data['quota'] == 0]
Data = Data.loc[Data['used'] != 0]
print(Data)

